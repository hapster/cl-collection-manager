(in-package :cl-collection-manager)

(defun list-entry-slots (entry)
  (mapcar #'slot-definition-name
	  (class-direct-slots
	   (class-of entry))))

(defun entry-already-present-p (entry collection)
  "Returns t if ENTRY is already a part of COLLECTION.")

;; (defun add-entry-to-collection (entry collection)
;;   (unless (entry-already-present-p entry collection)
;;     (push entry collection)))

(defgeneric add-collection (collection parent)
  (:documentation "Add collection to parent, provided it does not exist yet."))

(defmethod add-collection (collection parent)
  (unless (entry-already-present-p entry collection)
    (push collection parent)))

(defclass collection ()
  ((index
    :initarg :index
    :accessor index
    :documentation "A running number of all collections in use.")
   (relation
    :initarg :relation
    :accessor relation
    :documentation "Relation or relations the current collection is weaved within.")))

(defclass song (collection)
  ((artist
    :initarg :artist
    :accessor artist)
   (title
    :initarg :title
    :accessor title)))

(defgeneric retrieve-information (collection)
  (:documentation "Retrieve information of the collection."))

(defmethod retrieve-information (collection)
  (with-accessors ((index index) (relation relation)) collection
    (list index relation)))
