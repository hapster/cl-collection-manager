(asdf:defsystem #:cl-collection-manager
  :version "0.0.1"
  :description "A package to manage collections."
  :serial t
  :license "GPLv3"
  :components
  ((:file "package")
   (:file "collection-manager"))
  :depends-on (:cl-slug))
